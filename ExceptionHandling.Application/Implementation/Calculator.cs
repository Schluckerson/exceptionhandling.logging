﻿using System.Linq;
using Calculation.Interfaces;

namespace Calculation.Implementation
{
	public class Calculator : BaseCalculator, ICalculator
	{
		ILogger Logger { get; }
		public Calculator(ILogger logger)
		{
			Logger = logger;
		}

		public int Sum(params int[] numbers)
		{
			int sum = 0;
			Logger.Trace($"Начало {nameof(Sum)} с параметрами {System.String.Join(" ", numbers)}");
			try
            {
				sum = SafeSum(numbers);				
			}
			catch(System.Exception ex)
            {
				Logger.Error(ex);
				throw ;
			}
			finally
            {
				Logger.Trace($"Конец {nameof(Sum)}");			
			}
			Logger.Info($"Успешное выполнение {nameof(Sum)} с параметрами {System.String.Join(" ", numbers)} и результатом {sum}");
			return sum;
		}

		public int Sub(int a, int b)
		{
			try
            {
				return SafeSub(a, b);
			}
			catch (System.Exception ex)
            {
				throw ex;
			}			
		}

		public int Multiply(params int[] numbers)
		{
            try 
			{
				if (!numbers.Any())
					return 0;
				return SafeMultiply(numbers);
			}
			catch(System.Exception ex)
            {			
				throw new System.InvalidOperationException(ex.Message, ex);
            }
		}

		public int Div(int a, int b)
		{
            try
            {
				return a / b;
			}
			catch
            {
				throw new System.InvalidOperationException();
            }
		}
	}
}