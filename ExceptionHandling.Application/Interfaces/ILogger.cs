﻿using System;

namespace Calculation.Interfaces
{
	public interface ILogger
	{
		private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
		void Error(Exception ex)
		{
			Logger.Error(ex);
		}
		void Info(string message)
		{
			Logger.Info(message);
		}
		void Trace(string message)
        {
			Logger.Trace(message);
        }
	}
}